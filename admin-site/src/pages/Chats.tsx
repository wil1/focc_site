import {
    IonAlert,
    IonAvatar,
    IonButton,
    IonButtons,
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCardTitle,
    IonCol,
    IonContent,
    IonGrid,
    IonHeader,
    IonIcon,
    IonImg,
    IonItem,
    IonLabel,
    IonList,
    IonMenuButton,
    IonPage,
    IonRouterLink,
    IonRow,
    IonText,
    IonTextarea,
    IonTitle,
    IonToolbar,
} from '@ionic/react'
import gql from 'graphql-tag'
import { person } from 'ionicons/icons'
import React, { useContext, useState } from 'react'
import { Route } from 'react-router'
import { useMutation, useSubscription } from 'urql'
import { ShowError } from '../components/DevTools'
import { AppContext, User } from '../contexts/AppContext'

const subscribeChats = gql`
    subscription Chats {
        chats(orderBy: ID_ASC) {
            nodes {
                id
                name
            }
        }
    }
`

const addChat = gql`
    mutation CreateChat($name: String!) {
        __typename
        createChat(input: { chat: { name: $name } }) {
            chat {
                id
            }
        }
    }
`

const subscribeChatMessages = gql`
    subscription ChatMessages($chatId: BigInt!) {
        chats(condition: { id: $chatId }) {
            nodes {
                name
                chatMessages(orderBy: ID_ASC) {
                    nodes {
                        id
                        message
                        user {
                            id
                            preferredUsername
                            avatar
                        }
                    }
                }
            }
        }
    }
`
const addChatMessage = gql`
    mutation addChatMessage(
        $chatId: BigInt!
        $userId: BigInt!
        $message: String!
    ) {
        createChatMessage(
            input: {
                chatMessage: {
                    chatId: $chatId
                    message: $message
                    userId: $userId
                }
            }
        ) {
            chatMessage {
                id
            }
        }
    }
`
const ChatMessage: React.FC<any> = ({ msg }) => {
    const { preferredUsername, avatar } = msg.user
    return (
        <IonItem key={msg.id}>
            <IonAvatar slot="start">
                {avatar ? (
                    <IonImg src={avatar} />
                ) : (
                    <IonIcon size="large" icon={person} />
                )}
            </IonAvatar>
            <IonLabel>
                <h2>{preferredUsername}</h2>
                <p>{msg.message}</p>
            </IonLabel>
        </IonItem>
    )
}

const NewChatButton: React.FC = () => {
    const [alertOpen, setAlertOpen] = useState(false)
    const [addResponse, executeAdd] = useMutation(addChat)
    return (
        <>
            <IonButton
                disabled={addResponse.fetching}
                onClick={() => {
                    setAlertOpen(true)
                }}
            >
                New Chat
            </IonButton>
            <IonAlert
                isOpen={alertOpen}
                onDidDismiss={event => {
                    const { data, role } = event.detail
                    if (role !== `cancel`) {
                        executeAdd({ name: data.values.chatName })
                    }
                    setAlertOpen(false)
                }}
                header={'New Chat'}
                inputs={[
                    {
                        name: 'chatName',
                        type: 'text',
                        placeholder: 'Chat Name',
                    },
                ]}
                buttons={[
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary',
                    },
                    {
                        text: 'Ok',
                        role: 'submit',
                    },
                ]}
            />
        </>
    )
}

const ChatList: React.FC = () => {
    const [res] = useSubscription({
        query: subscribeChats,
        variables: {},
    })

    if (res.error) {
        return <ShowError message="Error loading chat list" data={res.error} />
    }
    const chats = res.data ? res.data.chats.nodes : []
    return (
        <IonCard>
            <IonList>
                {chats.map((chat: any) => {
                    const link = `/chats/${chat.id}`
                    return (
                        <IonItem key={chat.id} routerLink={link}>
                            <IonText>{chat.name}</IonText>
                        </IonItem>
                    )
                })}
            </IonList>
            <IonToolbar>
                <IonButtons>
                    <NewChatButton />
                </IonButtons>
            </IonToolbar>
        </IonCard>
    )
}

const ChatScreen: React.FC<{ user: User; chatId: number }> = ({
    user,
    chatId,
}) => {
    const [draft, setDraft] = useState({ message: `` })
    const [addResponse, executeAdd] = useMutation(addChatMessage)
    const [subscribeResponse] = useSubscription({
        query: subscribeChatMessages,
        variables: { chatId: chatId },
    })

    if (subscribeResponse.error) {
        return (
            <IonText color="danger">
                {JSON.stringify(subscribeResponse.error)}
            </IonText>
        )
    }

    const chat = subscribeResponse.data
        ? subscribeResponse.data.chats.nodes[0]
        : {}
    const messages = chat.chatMessages ? chat.chatMessages.nodes : []

    return (
        <IonCard>
            <IonCardHeader>
                <IonCardTitle>Chat: {chat.name}</IonCardTitle>
            </IonCardHeader>
            <IonCardContent>
                <IonGrid>
                    <IonRow>
                        <IonCol size="12">
                            <IonList>
                                {messages.map((msg: any) => (
                                    <ChatMessage key={msg.id} msg={msg} />
                                ))}
                            </IonList>
                        </IonCol>
                    </IonRow>
                    {addResponse && addResponse.error && (
                        <IonRow>
                            <IonCol size="12">
                                <ShowError
                                    message="An error occurred while sending the message"
                                    data={addResponse}
                                />
                            </IonCol>
                        </IonRow>
                    )}
                    <IonRow>
                        <IonCol size="9">
                            <IonTextarea
                                placeholder="Enter message..."
                                value={draft.message}
                                onIonChange={e => {
                                    setDraft({
                                        ...draft,
                                        message: e.detail.value || ``,
                                    })
                                }}
                            ></IonTextarea>
                        </IonCol>
                        <IonCol size="3">
                            <IonButton
                                onClick={e => {
                                    console.log(
                                        `Sending message: ${draft.message}`
                                    )
                                    executeAdd({
                                        chatId: chatId,
                                        userId: user.id,
                                        message: draft.message,
                                    })
                                    setDraft({ ...draft, message: `` })
                                }}
                                disabled={
                                    addResponse.fetching ||
                                    draft.message.length === 0
                                }
                            >
                                Send
                            </IonButton>
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonCardContent>
        </IonCard>
    )
}

const ChatPage: React.FC = () => {
    const { user } = useContext(AppContext)

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton />
                    </IonButtons>
                    <IonTitle>Chat</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent>
                {user ? (
                    <>
                        <Route
                            path={`/chats/:chatId`}
                            render={props => {
                                return (
                                    <ChatScreen
                                        user={user}
                                        chatId={parseInt(
                                            props.match.params.chatId
                                        )}
                                    />
                                )
                            }}
                        />
                        <Route
                            path={`/chats`}
                            exact={true}
                            render={(props: any) => {
                                return <ChatList />
                            }}
                        />
                    </>
                ) : (
                    <IonCard>
                        <IonCardHeader>
                            <IonCardTitle>Login required</IonCardTitle>
                        </IonCardHeader>
                        <IonCardContent>
                            <IonRouterLink routerLink="/login">
                                Log in
                            </IonRouterLink>
                        </IonCardContent>
                    </IonCard>
                )}
            </IonContent>
        </IonPage>
    )
}

export default ChatPage
