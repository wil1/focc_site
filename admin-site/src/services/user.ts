import { KeycloakProfile } from "keycloak-js"
import { useQuery } from "urql"

const getUser = `
query getUser($name String!) {
    allUsers(condition: {name: $name}) {
      nodes {
        id
        name
        avatar
      }
    }
  }
  
`

// const addChatMessage = `
//   mutation addChatMessage($message: String!) {
//     createChatMessage(
//       input: { chatMessage: { chatId: 1, message: $message, userId: 3 } }
//     ) {
//       chatMessage {
//         id
//       }
//     }
//   }
// `

// export const getProfile = async (name: string) => {
//     const [res] = useQuery({
//         query: getAllChatMessages,
//         variables: {},
//     })
    
// }
