import React from 'react'
import ReactJson from 'react-json-view'
import { IonText } from '@ionic/react'

export const ShowDebug: React.FC<any> = props => {
    console.log(`DEBUG`, props)
    return <div key={props.key}><ReactJson key={props.key} src={JSON.parse(JSON.stringify(props))} /></div>
}

export const ShowError: React.FC<{ message: string; data: any }> = (
    message,
    data
) => {
    console.error(message, data)
    return (
        <>
            <IonText color="danger">{message}</IonText>
            <ReactJson src={JSON.parse(JSON.stringify(data))} />
        </>
    )
}
