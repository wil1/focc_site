import {
    IonApp,
    IonLoading,
    IonPage,
    IonRouterOutlet,
    IonSplitPane,
} from '@ionic/react'
import { IonReactRouter } from '@ionic/react-router'
/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css'
import '@ionic/react/css/display.css'
import '@ionic/react/css/flex-utils.css'
import '@ionic/react/css/float-elements.css'
/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css'
/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css'
import '@ionic/react/css/structure.css'
import '@ionic/react/css/text-alignment.css'
import '@ionic/react/css/text-transformation.css'
import '@ionic/react/css/typography.css'
import {
    KeycloakEventHandler,
    KeycloakProvider,
    useKeycloak,
} from '@react-keycloak/web'
import gql from 'graphql-tag'
import { bug, chatboxes, code, home } from 'ionicons/icons'
import Keycloak from 'keycloak-js'
import React, { useEffect, useState } from 'react'
import { Redirect, Route } from 'react-router-dom'
import { SubscriptionClient } from 'subscriptions-transport-ws'
import {
    createClient,
    debugExchange,
    fetchExchange,
    Provider as UrqlClientProvider,
    subscriptionExchange,
    useMutation,
} from 'urql'
import Menu from './components/Menu'
import { AppContext, AppContextProps } from './contexts/AppContext'
import { AppPage } from './declarations'
import Chats from './pages/Chats'
import { DebugPage } from './pages/Debug'
import { GraphiqlPage } from './pages/Graphiql'
import Home from './pages/Home'
/* Theme variables */
import './theme/variables.css'

const realm = `focc_site`
const baseDomain = `dev.friendsofcc.org`

const appPages: AppPage[] = [
    {
        title: 'Home',
        url: '/home',
        icon: home,
    },
    {
        title: 'Chats',
        url: '/chats',
        icon: chatboxes,
    },
    {
        title: 'Debug',
        url: '/debug',
        icon: bug,
    },
    {
        title: 'GraphiQL',
        url: '/graphiql',
        icon: code,
    },
]
const keycloakConfig = {
    url: `https://keycloak.${baseDomain}/auth`,
    realm: realm,
    clientId: 'ionic-app',
}

const KeycloakLoading: React.FC = () => (
    <IonApp>
        <IonPage>
            <IonLoading isOpen={true} />
        </IonPage>
    </IonApp>
)
const KeycloakLogout: React.FC = () => {
    const [keycloak] = useKeycloak()
    useEffect(() => {
        if (keycloak.authenticated) {
            keycloak.logout()
        }
    })
    return <Redirect to="/" />
}
const KeycloakLogin: React.FC = () => {
    const [keycloak] = useKeycloak()
    useEffect(() => {
        if (!keycloak.authenticated) {
            keycloak.login({ scope: 'profile' })
        }
    })
    return <Redirect to="/" />
}
const keycloakInstance = Keycloak(keycloakConfig)

const baseGraphqlUrl = `://api.${baseDomain}/graphql`

const subscriptionClient = new SubscriptionClient(`wss${baseGraphqlUrl}`, {})

const client = createClient({
    url: `https${baseGraphqlUrl}`,
    fetchOptions: () => {
        return keycloakInstance.token
            ? {
                  credentials: 'include',
                  headers: {
                      Authorization: `Bearer ${keycloakInstance.token}`,
                  },
              }
            : {}
    },
    exchanges: [
        debugExchange,
        /*
        // cacheExchange is disabled by default
        cacheExchange,
        */
        fetchExchange,
        subscriptionExchange({
            forwardSubscription: operation =>
                subscriptionClient.request(operation),
        }),
    ],
})

type AppContextState = {
    authenticated: boolean
    registered: boolean
    contextProps: AppContextProps
}
const AppContextProvider: React.FC = props => {
    const [state, setState] = useState<AppContextState>({
        authenticated: false,
        registered: false,
        contextProps: {},
    })
    const [registerResponse, register] = useMutation(gql`
        mutation RegisterUser {
            registerUser(input: {}) {
                users {
                    id
                    externalId
                    preferredUsername
                    avatar
                }
            }
        }
    `)

    const handleKeycloakEvent: KeycloakEventHandler = (event, error) => {
        console.log('onKeycloakEvent', event, error)
        const nextState = { ...state }
        let changed = false
        if (nextState.contextProps.token !== keycloakInstance.token) {
            nextState.contextProps.token = keycloakInstance.token
            changed = true
        }
        if (event === 'onAuthSuccess') {
            nextState.authenticated = true
            changed = true
        }
        if (changed) {
            setState({ ...nextState, authenticated: true })
        }
    }

    if (
        state.authenticated &&
        !state.registered &&
        !registerResponse.fetching
    ) {
        register({})
    }

    if (registerResponse.data && !state.contextProps.user) {
        const user = registerResponse.data.registerUser.users[0]
        console.log(`User registered`, user)
        setState({
            ...state,
            registered: true,
            contextProps: { ...state.contextProps, user: user },
        })
    }
    return (
        <KeycloakProvider
            keycloak={keycloakInstance}
            onEvent={handleKeycloakEvent}
            LoadingComponent={<KeycloakLoading />}
        >
            <AppContext.Provider value={state.contextProps}>
                {props.children}
            </AppContext.Provider>
        </KeycloakProvider>
    )
}

const App: React.FC = () => {
    return (
        <UrqlClientProvider value={client}>
            <AppContextProvider>
                <IonApp>
                    <IonReactRouter>
                        <IonSplitPane
                            contentId="main"
                            when="(min-width: 3840px)"
                        >
                            <Menu appPages={appPages} />
                            <IonRouterOutlet id="main">
                                <Route
                                    path="/home"
                                    component={Home}
                                    exact={true}
                                />
                                <Route path="/chats" component={Chats} />
                                <Route
                                    path="/debug"
                                    render={() => (
                                        <DebugPage baseDomain={baseDomain} />
                                    )}
                                    exact={true}
                                />
                                <Route
                                    path="/graphiql"
                                    render={() => (
                                        <GraphiqlPage baseDomain={baseDomain} />
                                    )}
                                    exact={true}
                                />
                                <Route
                                    path="/login"
                                    component={KeycloakLogin}
                                    exact={true}
                                />
                                <Route
                                    path="/logout"
                                    component={KeycloakLogout}
                                    exact={true}
                                />
                                <Route
                                    path="/"
                                    render={() => <Redirect to="/home" />}
                                    exact={true}
                                />
                            </IonRouterOutlet>
                        </IonSplitPane>
                    </IonReactRouter>
                </IonApp>
            </AppContextProvider>
        </UrqlClientProvider>
    )
}

export default App
