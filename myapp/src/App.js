import React, { useState, useCallback, useEffect } from 'react'
import logo from './logo.svg'
import './App.css'

const Things = ({ things, setThings, limit, children }) => {
    const [draft, setDraft] = useState(``)

    const bind = title => ({ style: { border: `solid 1px red` }, title: title })

    return (
        <>
            <input
                {...bind(`The input`)}
                placeholder="Enter thing"
                onChange={e => setDraft(e.target.value)}
                value={draft}
            />
            <button
                disabled={draft.length === 0 || things.length >= limit}
                onClick={() => {
                    setThings([{id: draft, title: draft}, ...things])
                    setDraft(``)
                }}
                {...bind}
            >
                Add
            </button>
            <ul {...bind(`UL`)}>
                {things.map(thing => (
                    <li key={thing.id}>{thing.title}</li>
                ))}
            </ul>
            {things.length >= limit && children}
        </>
    )
}

function App() {
    const [things, setThings] = useState([])
    const [reset, setReset] = useState(0)

    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/todos')
            .then(response => response.json())
            .then(json => setThings(json))
    }, [reset])

    return (
        <>
          <button onClick={() => setReset(reset+1)}>reset</button>
          <Things limit={300} things={things} setThings={setThings}>
              <div>mooooo</div>
          </Things>
        </>
    )
}

export default App
