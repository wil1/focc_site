import {
    IonContent,
    IonHeader,
    IonIcon,
    IonItem,
    IonLabel,
    IonList,
    IonMenu,
    IonMenuToggle,
    IonTitle,
    IonToolbar,
    IonImg,
} from '@ionic/react'
import React from 'react'
import { RouteComponentProps, withRouter } from 'react-router-dom'
import { AppPage } from '../declarations'
import './Menu.css'

interface MenuProps extends RouteComponentProps {
    appPages: AppPage[]
}

const Menu: React.FunctionComponent<MenuProps> = ({ appPages }) => {
    return (
        <IonMenu contentId="main" type="overlay">
            <IonHeader>
                <IonToolbar>
                    <IonImg src="assets/focc-logo.svg" className="logo">Menu</IonImg>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <IonList>
                    {appPages.map((appPage, index) => {
                        return (
                            <IonMenuToggle key={index} autoHide={false}>
                                <IonItem
                                    routerLink={appPage.url}
                                    routerDirection="none"
                                >
                                    <IonIcon slot="start" icon={appPage.icon} />
                                    <IonLabel>{appPage.title}</IonLabel>
                                </IonItem>
                            </IonMenuToggle>
                        )
                    })}
                </IonList>
            </IonContent>
        </IonMenu>
    )
}
export default withRouter(Menu)
