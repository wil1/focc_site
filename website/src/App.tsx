import { IonApp, IonRouterOutlet, IonSplitPane } from '@ionic/react'
import { IonReactRouter } from '@ionic/react-router'
/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css'
import '@ionic/react/css/display.css'
import '@ionic/react/css/flex-utils.css'
import '@ionic/react/css/float-elements.css'
/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css'
/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css'
import '@ionic/react/css/structure.css'
import '@ionic/react/css/text-alignment.css'
import '@ionic/react/css/text-transformation.css'
import '@ionic/react/css/typography.css'
import { people, mail, code } from 'ionicons/icons'
import React from 'react'
import { Redirect, Route } from 'react-router-dom'
import { SubscriptionClient } from 'subscriptions-transport-ws'
import {
    createClient,
    debugExchange,
    fetchExchange,
    // Provider as UrqlClientProvider,
    subscriptionExchange,
} from 'urql'
import Menu from './components/Menu'
import { AppPage } from './declarations'
import { AboutPage } from './pages/About'
import { ContactPage } from './pages/Contact'
import { MixedPage } from './pages/Mixed'
/* Theme variables */
import './theme/variables.css'

const realm = `focc_site`
const baseDomain = `dev.friendsofcc.org`

const appPages: AppPage[] = [
    {
        title: 'About',
        url: '/about',
        icon: people,
    },
    {
        title: 'Contact',
        url: '/contact',
        icon: mail,
    },
    // {
    //     title: 'Mixed HTML Content',
    //     url: '/mixed',
    //     icon: code,
    // },
]

const baseGraphqlUrl = `://api.${window.location.hostname}/graphql`

const subscriptionClient = new SubscriptionClient(`wss${baseGraphqlUrl}`, {})

const client = createClient({
    url: `https${baseGraphqlUrl}`,
    exchanges: [
        debugExchange,
        /*
        // cacheExchange is disabled by default
        cacheExchange,
        */
        fetchExchange,
        subscriptionExchange({
            forwardSubscription: operation =>
                subscriptionClient.request(operation),
        }),
    ],
})

const App: React.FC = () => {
    return (
        // <UrqlClientProvider value={client}>
        <IonApp>
            <IonReactRouter>
                <IonSplitPane contentId="main" when="(min-width: 1280px)">
                    <Menu appPages={appPages} />
                    <IonRouterOutlet id="main">
                        <Route
                            path="/contact"
                            component={ContactPage}
                            exact={true}
                        />
                        <Route path="/mixed" component={MixedPage} exact={true} />
                        <Route path="/about" component={AboutPage} exact={true} />
                        <Route
                            path="/"
                            render={() => <Redirect to="/about" />}
                            exact={true}
                        />
                    </IonRouterOutlet>
                </IonSplitPane>
            </IonReactRouter>
        </IonApp>
        // </UrqlClientProvider>
    )
}

export default App
