import { IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonList, IonItem, IonLabel, IonInput, IonTextarea, IonIcon, IonButton, IonButtons, IonToolbar, IonToast } from '@ionic/react'
import React, { useState } from 'react'
import { BasePage } from './BasePage'
import './Contact.css'
import { send } from 'q'


type ContactState = {
    name: string
    email: string
    subject: string
    message: string,
    sent: string
}
function newContact<ContactState >() {
    return {
    name: ``,
    email: ``,
    subject: ``,
    message: ``,
    sent: `false`
}}
const blankContact = newContact()

function completed(input?: string) {
    return input && input.trim().length > 0
}

export const ContactPage: React.FC = () => {
    const [state, setState] = useState(blankContact)
    function makeInputProps(fieldName: keyof ContactState) {
        const fieldValue = state[fieldName]
        const handler = (e:any) => setState({...state, [fieldName]: e.detail.value || ''})
        return {
            name: fieldName as string, 
            value: fieldValue,
            onIonChange: handler
        }
    }
    
    const name = makeInputProps('name')
    const email = makeInputProps('email')
    const subject = makeInputProps('subject')
    const message = makeInputProps('message')

    function isValid() {
        return completed(name.value) && 
        completed(email.value) &&
        completed(subject.value) &&
        completed(message.value) 
    }
    
    return (
        <BasePage>
            <IonCard>
                <IonCardHeader>
                    <IonCardTitle>Contact Us</IonCardTitle>
                </IonCardHeader>
                <IonCardContent>
                <IonToast
        isOpen={state.sent === `true`}
        onDidDismiss={() => setState({...state, sent: `dismissed`})}
        message="Message sent."
        duration={5000}
      />

                    <IonList>
                        <IonItem>
                            <IonLabel position="stacked">Name</IonLabel>
                            <IonInput {...name}></IonInput>
                        </IonItem>
                        <IonItem>
                            <IonLabel position="stacked">Email Address</IonLabel>
                            <IonInput {...email}></IonInput>
                        </IonItem>
                        <IonItem>
                            <IonLabel position="stacked">Subject</IonLabel>
                            <IonInput {...subject}></IonInput>
                        </IonItem>
                        <IonItem>
                            <IonLabel position="stacked">Message</IonLabel>
                            <IonTextarea {...message}></IonTextarea>
                        </IonItem>
                        <IonItem>
                        <IonButton
                                onClick={e => {
                                    console.log(
                                        `Sending message`, state
                                    )
                                    // executeAdd({
                                    //     chatId: chatId,
                                    //     userId: user.id,
                                    //     message: draft.message,
                                    // })
                                    setState({...state, sent: `true`})
                                }}
                                disabled={
                                    // addResponse.fetching ||
                                    !isValid()
                                }
                            >
                                Send
                            </IonButton>
                        </IonItem>
                    </IonList>
                </IonCardContent>
            </IonCard>
        </BasePage>
    )
}
