import {
    IonCol,
    IonContent,
    IonGrid,
    IonPage,
    IonRow,
    IonToggle,
} from '@ionic/react'
import React, { useState } from 'react'
import './Mixed.css'

export const MixedPage: React.FC = () => {
    const [toggleValue, setToggleValue] = useState(false)
    return (
        <IonPage>
            <IonContent>
                <IonGrid>
                    <IonRow>
                        <IonCol size="6">A</IonCol>
                        <IonCol size="3">B</IonCol>
                        <IonCol size="3">C</IonCol>
                    </IonRow>
                    <IonRow>
                        <IonCol size="9">
                            <div className="my-big-div">
                                <p>Regular HTML can be mixed with Ionic tags</p>
                                <p
                                    className={
                                        toggleValue ? 'toggled-class' : ''
                                    }
                                >
                                    The style changes when toggled
                                </p>
                            </div>
                        </IonCol>
                        <IonCol size="3">
                            <IonToggle
                                checked={toggleValue}
                                onIonChange={event =>
                                    setToggleValue(!toggleValue)
                                }
                            />
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonContent>
        </IonPage>
    )
}
