import {
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCardTitle,
} from '@ionic/react'
import React from 'react'
import YouTube from 'react-youtube-embed'
import './About.css'
import { BasePage } from './BasePage'

export const AboutPage: React.FC = () => {
    return (
        <BasePage>
            <IonCard>
                <IonCardHeader>
                    <IonCardTitle>
                        About Us
                    </IonCardTitle>
                </IonCardHeader>
                <IonCardContent>
                    <YouTube id="lpHzZQFz2rg" />
                </IonCardContent>
            </IonCard>

            <IonCard>
                <IonCardHeader>
                    <IonCardTitle>Our Mission</IonCardTitle>
                </IonCardHeader>
                <IonCardContent>
                    Our mission is to support Community Connections and promote
                    empathetic communication skills and restorative practices.
                    These skills and practices increase cooperation, transform
                    conflict, and create connection.
                </IonCardContent>
            </IonCard>

            <IonCard>
                <IonCardHeader>
                    <IonCardTitle>Our Aim</IonCardTitle>
                </IonCardHeader>
                <IonCardContent>
                    Our aim is to help Community Connections reach more people
                    in the community and, particularly, in the schools. We
                    intend to talk with local leaders to gain support to expand
                    and we plan to raise funds to expand the model.
                </IonCardContent>
            </IonCard>
        </BasePage>
    )
}
