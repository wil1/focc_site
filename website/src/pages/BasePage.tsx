import {
    IonButtons,
    IonContent,
    IonHeader,
    IonMenuButton,
    IonPage,
    IonTitle,
    IonToolbar,
} from '@ionic/react'
import React from 'react'
import './BasePage.css'

export type BasePageProps = {
    title?: string
}

export const BasePage: React.FC<BasePageProps> = props => {
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton />
                    </IonButtons>
                    <IonTitle>
                        {props.title || `Friends of Community Connections`}
                    </IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>{props.children}</IonContent>
        </IonPage>
    )
}
