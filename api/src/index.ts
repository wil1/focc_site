import express = require("express")
import cors = require("cors")
import path = require("path")
import PgPubsub from "@graphile/pg-pubsub"
import * as log from "consola"
import * as jwt from "express-jwt"
import { readFileSync } from "fs"
import * as jwksRsa from "jwks-rsa"
import * as jwtDecode from "jwt-decode"
import { Client } from "pg"
import {
  GraphQLErrorExtended,
  makePluginHook,
  postgraphile,
} from "postgraphile"
import * as PostgraphileLogConsola from "postgraphile-log-consola"
import { extendedFormatError } from "postgraphile/build/postgraphile/extendedFormatError"
import { migrate } from "postgres-migrations"

const env = (key: string, defaultValue?: string) => {
  if (process.env[key]) {
    return process.env[key]
  }
  if (defaultValue) {
    return defaultValue
  }
  throw new Error(`Missing environment variable: ${key}`)
}


const stackName = env(`STACK_NAME`)
const baseDomain = env(`BASE_DOMAIN`)
const databaseName = stackName
const postgresUser = stackName
const postgresPassword = readFileSync(
  `/run/secrets/${stackName}_admin_password`
)
  .toString()
  .replace(/^\s+|\s+$/g, "")

const postgresHost = env(`POSTGRES_HOST`, `postgres`)
const postgresPort = parseInt(env(`POSTGRES_PORT`, `5432`))

const schema = env(`DATABASE_SCHEMA`, `app`)

const postgresUrl = `postgres://${postgresUser}:${postgresPassword}@${postgresHost}:${postgresPort}/${databaseName}`

const migrationDirectory = env(`MIGRATION_DIRECTORY`, `./migrations`)

const readSqlScriptFile = (name: string) =>
  readFileSync(path.resolve(__dirname, `./sql/${name}`)).toString()

async function loadSqlScripts(...scripts: string[]) {
  const client = new Client({ connectionString: postgresUrl })
  await client.connect()
  try {
    for (var i = 0; i < scripts.length; i++) {
      log.info(`loading application SQL script ${scripts[i]}`)
      const sql = readSqlScriptFile(scripts[i])
      await client.query(sql)
    }
  } finally {
    await client.end()
  }
}

// TODO remove
async function resetDB() {
  log.info(`resetting database: ${databaseName}`)
  const query = `
    drop schema if exists app cascade;
    drop schema if exists app_hidden cascade;
    drop table if exists migrations;
    
    drop user if exists app;
    drop user if exists app_user;
  `
  const client = new Client({ connectionString: postgresUrl })
  await client.connect()
  try {
    await client.query(query)
  } finally {
    await client.end()
  }
}

async function initDatabase() {
  await resetDB();
  log.info(`initializing database: ${databaseName}`)
  let migrations = await migrate(
    {
      database: databaseName,
      user: postgresUser,
      password: postgresPassword,
      host: postgresHost,
      port: postgresPort,
    },
    migrationDirectory,
    {
      logger: (msg) => log.info(msg),
    }
  )
  log.info(`migrations complete`)
  await loadSqlScripts("app.sql")
  log.info(`database initialized`)
}

const realm = env(`REALM`, stackName)

const checkJwt = jwt({
  secret: jwksRsa.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: `https://keycloak.${baseDomain}/auth/realms/${realm}/protocol/openid-connect/certs`,
  }),
  audience: `account`,
  issuer: `https://keycloak.${baseDomain}/auth/realms/${realm}`,
  algorithms: ["RS256"],
  credentialsRequired: false,
})

const whitelist = [`https://api.${baseDomain}`, `https://admin.${baseDomain}`, `https://${baseDomain}`]
const corsOptions = {
  origin: function (origin, callback) {
    log.info(`Checking origin: ${origin}`)
    if (origin && whitelist.indexOf(origin) == -1) {
      callback(new Error(`Not allowed by CORS: ${origin}`))
    } else {
      callback(null, true)
    }
  },
  credentials: true,

  allowedHeaders: [
    "Origin",
    "X-Requested-With",
    // Used by `express-graphql` to determine whether to expose the GraphiQL
    // interface (`text/html`) or not.
    "Accept",
    // Used by PostGraphile for auth purposes.
    "Authorization",
    // Used by GraphQL Playground and other Apollo-enabled servers
    "X-Apollo-Tracing",
    // The `Content-*` headers are used when making requests with a body,
    // like in a POST request.
    "Content-Type",
    "Content-Length",
    // For our 'Explain' feature
    "X-PostGraphile-Explain",
  ],
}
const withCors = cors(corsOptions)

const bearerOffset = "bearer ".length
const getJwtFromRequest = (request: any) => {
  const auth = request.headers["authorization"]
  if (auth && auth.trim().length > bearerOffset) {
    const token = auth.trim().substr(bearerOffset)
    return jwtDecode(token)
  }
  return null
}

export const getPgSettings = async (request: any) => {
  const jwt = getJwtFromRequest(request)
  const settings = {}
  settings[`role`] = `app_user`
  settings[`search_path`] = schema
  if (jwt) {
    settings[`app.user_session.external_id`] = jwt.sub
    settings[`app.user_session.preferred_username`] = jwt.preferred_username
    settings[`app.user_session.roles`] = jwt.realm_access.roles
      .map(role => `(${role})`)
      .join(` `)
  }
  log.info(`Using PgSettings:`, settings)
  return settings
}

const PgSimplifyInflectorPlugin = require("@graphile-contrib/pg-simplify-inflector")
const SubscriptionPlugin = require("@graphile/subscriptions-lds").default
const pluginHook = makePluginHook([PostgraphileLogConsola, PgPubsub])

const extendedErrors = [
  "severity",
  "code",
  "detail",
  "hint",
  "position",
  "internalPosition",
  "internalQuery",
  "where",
  "schema",
  "table",
  "column",
  "dataType",
  "constraint",
  "file",
  "line",
  "routine",
]

const formatError = error => {
  const formattedError = extendedFormatError(error, extendedErrors)
  formattedError["stack"] = error.stack
  log.error(formattedError)
  return formattedError as GraphQLErrorExtended
}

async function start() {
  await initDatabase()
  const app = express()

  // Apply CORS to the graphql endpoint
  app.options("/graphql", withCors)
  app.use(
    withCors,
    postgraphile(postgresUrl, schema, {
      pluginHook,
      subscriptions: true, // start the websocket server
      simpleSubscriptions: true, // Add the `listen` subscription field
      live: true,
      watchPg: true,
      graphiql: true,
      enhanceGraphiql: true,
      ignoreRBAC: false,
      pgSettings: getPgSettings,
      handleErrors: errors => {
        return errors.map(formatError)
      },
      ownerConnectionString: postgresUrl,
      appendPlugins: [PgSimplifyInflectorPlugin, SubscriptionPlugin],
      graphileBuildOptions: {
        /*
         * Uncomment if you want simple collections to lose the 'List' suffix
         * (and connections to gain a 'Connection' suffix).
         */
        // pgOmitListSuffix: true,
        /*
         * Uncomment if you want 'userPatch' instead of 'patch' in update
         * mutations.
         */
        //pgSimplifyPatch: false,
        /*
         * Uncomment if you want 'allUsers' instead of 'users' at root level.
         */
        //pgSimplifyAllRows: false,
        /*
         * Uncomment if you want primary key queries and mutations to have
         * `ById` (or similar) suffix; and the `nodeId` queries/mutations
         * to lose their `ByNodeId` suffix.
         */
        // pgShortPk: true,
      },
    })
  )

  app.options("/decode-token", withCors)
  app.get(`/decode-token`, withCors, checkJwt, (req, res) => {
    const result = getJwtFromRequest(req)
    if (result) {
      res.json(result)
    } else {
      res.status(401)
    }
  })

  log.info(`starting express server`)
  app.listen(env(`PORT`, `3000`))
}

start()
