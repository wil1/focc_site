set search_path to app;

create or replace function register_user() returns setof users as $$
declare
    resultId bigint;
    externalId text;
    preferredUsername text;
begin
    select 
        external_id, preferred_username into externalId, preferredUsername
    from user_session;

    if externalId is null then
        raise exception 'app.user_session.external_id setting is null'
            using hint = 'app.user_session is populated from the JWT token';
    end if;

    select id into resultId 
    from users
    where external_id = externalId;
       
    if resultId is null then
        insert into users (external_id, preferred_username) 
        values (externalId, preferredUsername)
        returning id into resultId;
    else
        update users 
        set preferred_username = preferredUsername
        where id = resultId;
    end if;
       
    return query select * from users where id = resultId;
end;
$$ LANGUAGE plpgsql;


set search_path to public;
